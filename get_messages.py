from TwitterAPI import TwitterAPI

import ConfigParser
import services
import Queue
import time

config = ConfigParser.RawConfigParser()
config.read('config.cfg')

conf = dict(config.items('Twitter'))

consumer_key = conf['consumerkey']
consumer_secret = conf['consumersecret']
access_token_key = conf['accesstoken']
access_token_secret = conf['accesssecret']

api = TwitterAPI(consumer_key, consumer_secret, access_token_key, access_token_secret)


def get_messages():
    queue = Queue.Queue()
    for user in services.users():
        queue.put(user.username)
    while not queue.empty():
        username = queue.get()
        print username
        for item in api.request('statuses/user_timeline', {'screen_name': username, 'count': 200}):
            if item.get('code', 0) == 88:
                queue.put(username)
                print item.get('message')
                time.sleep(60)
                continue

            if 'error' in item:
                continue

            tweet = services.find_tweet(item['id_str'])
            if tweet:
                if 'retweeted_status' in item:
                    related_tweet = services.find_tweet(item['retweeted_status']['id_str'])
                    if related_tweet:
                        print '\tActualizado retweet'
                        tweet.related_tweet_id = related_tweet.id
                        tweet.tweet_type = 'retweet'
                        services.flush_session()
                elif 'in_reply_to_status_id' in item:
                    related_tweet = services.find_tweet(item['in_reply_to_status_id'])
                    if related_tweet:
                        print '\tActualizado reply'
                        tweet.related_tweet_id = related_tweet.id
                        tweet.tweet_type = 'response'
                        services.flush_session()
                continue

            user = services.find_user(username)
            related_tweet = None

            if 'in_reply_to_status_id' in item:
                related_tweet = services.find_tweet(item['in_reply_to_status_id'])

            if 'retweeted_status' in item:
                related_tweet = services.find_tweet(item['retweeted_status']['id_str'])
                services.add_tweet(item['id'], user, 'retweet', related_tweet=related_tweet)
                print '\tretweet'
            else:
                tweet = services.add_tweet(item['id'], user, 'regular', related_tweet=related_tweet)
                if 'user_mentions' in item.get('entities', {}):
                    tweet.tweet_type = 'response'
                    for mentioned in item['entities']['user_mentions']:
                        user_mentioned = services.find_user(mentioned['screen_name'])
                        if user_mentioned:
                            services.add_mention(tweet, mentioned['screen_name'])
                            print "\tMentioned: %s" % (mentioned['screen_name'])


if __name__ == '__main__':
    get_messages()