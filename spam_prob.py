from services import users, users_count, flush_session, tweets, mentions

n_users = users_count()
factors = (0.25, 0.4, 0.35)

maximum = 0
for user in users():
    n_replies, n_tweets = tweets(user)
    n_mentions, n_mentioned = mentions(user)
    t_mm = n_mentions + n_mentioned
    user.legitimacy =\
        factors[0] * user.importance +\
        factors[1] * n_replies / (n_tweets if n_tweets else 1) +\
        factors[2] * n_mentions / (t_mm if t_mm else 1)
    maximum = max(maximum, user.legitimacy)

# normalization
for user in users():
    user.legitimacy /= maximum

flush_session()

l_factor = 0.2
d_factor = 0.7
for i in range(2):
    cache = {}
    for user in users():
        acc1, acc2 = 0, 0
        for followed in user.following:
            acc1 += followed.legitimacy
        for follower in user.followers:
            acc2 += follower.legitimacy
        t1, t2 = len(user.following), len(user.followers)
        prom =\
            l_factor * acc1 / (t1 if t1 else 1) +\
            (1-l_factor) * acc2 / (t2 if t2 else 1)
        cache[user.username] = d_factor * user.legitimacy + (1-d_factor) * prom
    for user in users():
        user.legitimacy = cache[user.username]
    flush_session()
