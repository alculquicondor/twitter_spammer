#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import ConfigParser

from sqlalchemy import create_engine, Column, Float, Integer, String,\
    ForeignKey, Enum
from sqlalchemy.dialects.postgres import ARRAY
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.engine.url import URL as EngineURL
from sqlalchemy.ext.associationproxy import association_proxy

config = ConfigParser.RawConfigParser()
config.read('config.cfg')

engine = create_engine(EngineURL(**dict(config.items('SQLdatabase'))))
Base = declarative_base()
Session = sessionmaker(bind=engine)


class User(Base):
    __tablename__ = 'user'

    username = Column(String(250), primary_key=True, autoincrement=False)
    real_name = Column(String(250))
    importance = Column(Float)
    legitimacy = Column(Float)
    nb_following = Column(Integer, default=0)
    _following = relationship("Follow",
                              primaryjoin='User.username==Follow.follower_id')
    _followers = relationship("Follow",
                              primaryjoin='User.username==Follow.followed_id')
    following = association_proxy('_following', 'followed')
    followers = association_proxy('_followers', 'follower')

    def __repr__(self):
        return 'User<username=%r, real_name=%r>' %\
               (self.username, self.real_name)


class Follow(Base):
    __tablename__ = 'follow'

    follower_id = Column(String, ForeignKey('user.username'), primary_key=True,
                         index=True)
    follower = relationship("User",
                            primaryjoin='Follow.follower_id==User.username')
    followed_id = Column(String, ForeignKey('user.username'), primary_key=True,
                         index=True)
    followed = relationship("User",
                            primaryjoin='Follow.followed_id==User.username')

    def __repr__(self):
        return 'Follow<follower=%r, followed=%r>' % \
               (self.follower.username, self.followed.username)


class Tweet(Base):
    __tablename__ = 'tweet'
    tweet_type = Enum('regular', 'response', 'retweet', name='tweet_type')

    id = Column(String, primary_key=True, autoincrement=False)
    user_id = Column(String, ForeignKey('user.username'), nullable=False,
                     index=True)
    user = relationship('User')
    type = Column(tweet_type, nullable=False,
                  default=tweet_type.enums[0])
    links = Column(ARRAY(String))
    related_tweet_id = Column(String, ForeignKey('tweet.id'), index=True)
    related_tweet = relationship('Tweet')

    def __repr__(self):
        return 'Tweet<user=%r, type=%r>' %\
               (self.user.username, self.type)


class Mention(Base):
    __tablename__ = 'mention'

    id = Column(Integer, primary_key=True)
    tweet_id = Column(String, ForeignKey('tweet.id'), nullable=False)
    tweet = relationship('Tweet')
    mentioned_id = Column(String, ForeignKey('user.username'), nullable=False,
                          index=True)
    mentioned = relationship('User')

    def __repr__(self):
        return 'Mention<tweet=%r, mentioned=%r>' %\
               (self.tweet, self.mentioned)


def create_database():
    Base.metadata.create_all(engine)


if __name__ == '__main__':
    create_database()
