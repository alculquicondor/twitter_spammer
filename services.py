from models import Session, User, Follow, Tweet, Mention
from sqlalchemy import sql
import sqlalchemy.orm


_session = Session()
assert isinstance(_session, sqlalchemy.orm.Session)


def find_user(username):
    return _session.query(User).get(username)


def find_tweet(_id):
    return _session.query(Tweet).get(str(_id))


def find_follow(follower, followed):
    return _session.query(Follow).filter_by(
        follower=follower, followed=followed).first()


def add_user(username, real_name):
    user = find_user(username)
    if not user:
        user = User(username=username, real_name=real_name)
        _session.add(user)
        _session.commit()
    return user


def add_follow(follower, followed):
    if isinstance(follower, (str, unicode)):
        follower = find_user(follower)
        if follower is None:
            raise ValueError('User not found')
    if isinstance(followed, (str, unicode)):
        followed = find_user(followed)
        if followed is None:
            raise ValueError('User not found')
    follow = find_follow(follower, followed)
    if follow is None:
        follow = Follow(follower=follower, followed=followed)
        _session.add(follow)
        follower.nb_following += 1
        _session.commit()
    return follow


def add_tweet(tweet_id, user, tweet_type=Tweet.tweet_type.enums[0], links=None,
              related_tweet=None):
    if isinstance(user, (str, unicode)):
        user = find_user(user)
        if user is None:
            raise ValueError('User not found')
    tweet = Tweet(id=str(tweet_id), user=user, type=tweet_type, links=links)
    if related_tweet:
        tweet.related_tweet_id = related_tweet.id
    _session.add(tweet)
    _session.commit()
    return tweet


def add_mention(tweet, mentioned):
    if isinstance(mentioned, (str, unicode)):
        mentioned = find_user(mentioned)
        if mentioned is None:
            raise ValueError('User not found')
    mention = Mention(tweet=tweet, mentioned=mentioned)
    _session.add(mention)
    _session.commit()
    return mention


def flush_session():
    _session.commit()


def users_count():
    return _session.query(User).count()


def users():
    return _session.query(User).yield_per(10).enable_eagerloads(False)


def tweets(user):
    clause = sql.text(
        'SELECT count(t1.id) '
        'FROM "tweet" t1 JOIN "tweet" t2 ON t1.id = t2.related_tweet_id'
        '  JOIN "user" u ON t1.user_id = u.username '
        'WHERE u.username = :username')
    related = _session.execute(clause, params={'username': user.username})\
        .fetchone()[0]
    total = _session.query(Tweet).filter(Tweet.user_id == user.username).count()
    return related, total


def mentions(user):
    n_mention = _session.query(Mention).\
        filter(Mention.mentioned_id == user.username).count()
    n_mentioned = _session.query(Mention).join("tweet", "user").\
        filter(User.username == user.username).count()
    return n_mention, n_mentioned
