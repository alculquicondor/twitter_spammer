from services import users, users_count, flush_session

n_users = users_count()
d_factor = 0.85

for user in users():
    user.importance = 1. / n_users

flush_session()

for i in range(10):
    cache = {}
    for user in users():
        acc = 0
        for follower in user.followers:
            acc += follower.importance / follower.nb_following
        cache[user.username] = (1-d_factor) / n_users + acc * d_factor
    for user in users():
        user.importance = cache[user.username]
    flush_session()
