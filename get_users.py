from TwitterAPI import TwitterAPI

import ConfigParser
import Queue
import services
import re
import time

config = ConfigParser.RawConfigParser()
config.read('config.cfg')

conf = dict(config.items('Twitter'))

consumer_key = conf['consumerkey']
consumer_secret = conf['consumersecret']
access_token_key = conf['accesstoken']
access_token_secret = conf['accesssecret']

api = TwitterAPI(consumer_key, consumer_secret, access_token_key, access_token_secret)


def handle(message, queue, user_id, followed_by=None):
    print message
    queue.put((user_id, followed_by))
    time.sleep(60)  # 1 minute


def get_users(start_uid=3369151):  # ocram id
    queue = Queue.Queue()
    queue.put((start_uid, None))
    qsize = 1
    visited = {}
    while True:
        try:
            print 'DEBUG:', qsize
            user_id, followed_by = queue.get(timeout=1)
            qsize -= 1
        except Queue.Empty:
            print "finished"
            break

        response = api.request('users/lookup', {'user_id': user_id})
        save = False
        username = None
        for user in response:
            if user.get('code', 0) == 88:
                handle(user['message'], queue, user_id, followed_by)
            save = re.match(r'.*[Ll][Ii][Mm][Aa].*', user.get('location', ''))
            if save:
                username = services.add_user(user['screen_name'],
                                             user.get('name', '')).username
                print "%s - %s" % (user['screen_name'], user['name'])
        visited[user_id] = username  # possibly None

        if save:
            if followed_by:
                try:
                    services.add_follow(followed_by, username)
                except Exception:
                    pass
            response = api.request('friends/ids', {'user_id': user_id, 'count': 5000})
            if "errors" in response:
                handle(response['errors'], queue, user_id)
            for item in response:
                if item.get('code', 0) == 88:
                    handle(item['message'], queue, user_id, followed_by)
                for follower_id in item.get('ids', []):
                    if follower_id in visited:
                        if visited[follower_id]:
                            try:
                                services.add_follow(
                                    username,
                                    visited[follower_id])
                            except Exception:
                                pass
                    queue.put((follower_id, username))
                    qsize += 1

if __name__ == '__main__':
    from sys import argv
    if len(argv) > 1:
        get_users(int(argv[1]))
    else:
        get_users()
