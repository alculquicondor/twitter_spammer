__author__ = 'ratasxy'

from scrapy.http import Request, FormRequest
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import Rule, CrawlSpider


class TwitterSpider(CrawlSpider):
    name = "twitter"
    allowed_domains = ["twitter.com"]
    login_page = 'https://twitter.com/login'
    start_urls = ['https://twitter.com/ocram/followers']

    rules = (
        Rule(LinkExtractor(allow=("twitter.com/.*", ), ), callback="parse_items", follow=True),
    )

    def init_request(self):
        return Request(url=self.login_page, callback=self.login)

    def login(self, response):
        return FormRequest.from_response(
            response,
            formdata={'username': 'jcardenasgr', 'password': '123jcar4'},
            callback=self.check_login_response)

    def check_login_response(self, response):
        self.initialized()

    def parse_items(self, response):
        # for div in response.xpath('//span[contains(@class, "js-action-profile-name")]/text()'):
        #     print div.extract()
        #     print "\n\n"
        for tweet in response.xpath('//div[contains(@id, "stream-item-tweet")]'):
            name = tweet.xpath('div//b[contains(@class, "ProfileTweet-fullname")]/text()').extract()
            user = tweet.xpath('div//span[contains(@class, "ProfileTweet-screenname")]/text()[2]').extract()
            print "%s  --- %s\n" % (name, user)
