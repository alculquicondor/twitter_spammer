from services import users, flush_session

for user in users():
    user.nb_following = len(user.following)

flush_session()
